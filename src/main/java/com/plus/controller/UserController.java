package com.plus.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import com.plus.common.R;
import com.plus.utils.Func;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.alibaba.fastjson.JSON;
import java.util.List;
import com.plus.entity.User;
import com.plus.dto.UserDTO;

import com.plus.service.IUserService;

/**
 * 用户表 控制器
 *
 * @author AI
 * @since 2023-11-02
 *
 */
@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/api/user")
@Api(value = "/api/user", tags = "用户表相关接口")
public class UserController {



    private IUserService userService;
    @Autowired
    public void UserController(IUserService userService){
		this.userService = userService;
    }


	/**
	 * 详情
	 */
	@GetMapping("/detail")
	@ApiOperation(value = "/detail", notes = "详情-传入user")
	public R<User> detail(UserDTO dto) {
		User detail = userService.getOne(dto);
		return R.data(detail);
	}

	/**
	 * 分页 用户表
	 */
	@GetMapping("/page")
	@ApiOperation(value = "/page", notes = "分页-传入user")
	public R<IPage<User>> page(UserDTO dto) {
		IPage<User> pages = userService.page(dto);
		return R.data(pages);
	}
	/**
	 * 不分页 用户表
	 */
	@GetMapping("/list")
	@ApiOperation(value = "/list", notes = "不分页-传入user")
	public R<List<User>> list(UserDTO dto) {
		List<User> list = userService.list(dto);
		return R.data(list);
	}

	/**
	 * 新增 用户表
	 */
	@PostMapping("/save")
	@ApiOperation(value = "/save", notes = "新增-传入user")
	public R save(@RequestBody UserDTO dto) {
		return R.data(userService.save(dto));
	}

	/**
	 * 修改 用户表
	 */
	@PutMapping("/update")
	@ApiOperation(value = "/update", notes = "修改-传入user")
	public R update(@RequestBody UserDTO dto) {
		return R.data(userService.updateById(dto));
	}

	/**
	 * 删除 用户表
	 */
	@DeleteMapping("/remove")
	@ApiOperation(value = "/remove", notes = "逻辑删除-传入ids")
	public R remove(@ApiParam(value = "主键集合", required = true) @RequestParam String ids) {
		return R.data(userService.deleteLogic(Func.toIntList(ids)));
	}

}
