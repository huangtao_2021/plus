//package com.plus.utils;
//
//import org.apache.activemq.ScheduledMessage;
//import org.apache.activemq.command.ActiveMQQueue;
//import org.apache.activemq.command.ActiveMQTopic;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.autoconfigure.jms.JmsProperties;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.jms.core.JmsMessagingTemplate;
//
//import javax.annotation.PostConstruct;
//import javax.jms.*;
//import java.io.Serializable;
//
///**
// * @program: plus
// * @ClassName ActivemqUtil
// * @description: Activemq帮助类
// * @author: AI
// * @create: 2023-11-03 14:53
// * @Version 1.0
// **/
//@Configuration
//public class ActivemqUtil {
//
//
//    private static JmsMessagingTemplate jmsMessagingTemplate;
//    private JmsMessagingTemplate jmsMessagingTemplate_;
//    @Autowired
//    public void ActivemqUtil(JmsMessagingTemplate jmsMessagingTemplate){
//        this.jmsMessagingTemplate_ = jmsMessagingTemplate;
//    }
//
//    @PostConstruct
//    private void init() {
//        this.jmsMessagingTemplate = jmsMessagingTemplate_;
//    }
//
//
//    /**
//     *
//     * @param type 类型，队列还是主题
//     * @param activemqName  队列名称，生产者发送的队列名称要和消费者接收的队列名称一致
//     * @param msg 消息
//     * @param time 延迟时间
//     * @return
//     */
//    public static boolean SendMQ(String type,String activemqName,String msg,Long time){
//        Destination destination = null;
//        if (type == null) {
//            type = "";
//        }
//        switch (type) {
//            case "topic":
//                //发送广播消息
//                destination = new ActiveMQTopic(activemqName);
//                break;
//            default:
//                //发送 队列消息
//                destination = new ActiveMQQueue(activemqName);
//                break;
//        }
//        if (time != null && time > 0) {
//            //延迟队列，延迟time毫秒
//            //延迟队列需要在 <broker>标签上增加属性 schedulerSupport="true"
//            delaySend(destination, msg, time);
//        } else {
//            jmsMessagingTemplate.convertAndSend(destination, msg);//无序
//            //jmsMessagingTemplate.convertSendAndReceive();//有序
//        }
//        return true;
//    }
//
//
//    /**
//     * 延时发送
//     * 说明：延迟队列需要在 <broker>标签上增加属性 schedulerSupport="true"
//     *
//     * @param destination 发送的队列
//     * @param data        发送的消息
//     * @param time        延迟时间 /毫秒
//     */
//    public static <T extends Serializable> void delaySend(Destination destination, T data, Long time) {
//        Connection connection = null;
//        Session session = null;
//        MessageProducer producer = null;
//        // 获取连接工厂
//        ConnectionFactory connectionFactory = jmsMessagingTemplate.getConnectionFactory();
//        try {
//            // 获取连接
//            connection = connectionFactory.createConnection();
//            connection.start();
//            // 获取session，true开启事务，false关闭事务
//            session = connection.createSession(Boolean.TRUE, Session.AUTO_ACKNOWLEDGE);
//            // 创建一个消息队列
//            producer = session.createProducer(destination);
//            producer.setDeliveryMode(JmsProperties.DeliveryMode.PERSISTENT.getValue());
//            ObjectMessage message = session.createObjectMessage(data);
//            //设置延迟时间
//            message.setLongProperty(ScheduledMessage.AMQ_SCHEDULED_DELAY, time);
//            // 发送消息
//            producer.send(message);
//            session.commit();
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                if (producer != null) {
//                    producer.close();
//                }
//                if (session != null) {
//                    session.close();
//                }
//                if (connection != null) {
//                    connection.close();
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    }
//
//}
