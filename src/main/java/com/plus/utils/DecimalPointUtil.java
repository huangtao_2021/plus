package com.plus.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * @Description :数字处理工具
 */
public class DecimalPointUtil {

    /**
     * 方法名：formatToDouble
     * 功能：double类型数值保留两位小数,四舍五入
     * 描述：返回值为double
     */
    public static double formatToDouble(double number) {
        BigDecimal bigDecimal = new BigDecimal(number);
        return bigDecimal.setScale(2, BigDecimal.ROUND_UP).doubleValue();
    }

    /**
     * 方法名：formatToString
     * 功能：double类型的数值保留两位小数
     * 描述：返回值为String
     */
    public static String formatToString(double number) {
        DecimalFormat df = new DecimalFormat("#.00");
        return df.format(number);
    }

    /**
     * 方法名：formatToStr
     * 功能：double类型的数值保留两位小数
     * 描述：返回值为String
     */
    public static String formatToStr(double number) {
        return String.format("%.2f", number);
    }

    /**
     * 方法名：formatToStr
     * 功能：String类型的数值保留两位小数
     * 描述：返回值为String
     */
    public static String formatToStr(String str){
        if(str != null && str.length() > 0){
            DecimalFormat format = new DecimalFormat("0.00");
            BigDecimal bigDecimal = new BigDecimal(str);
            return format.format(bigDecimal);
        }
        return "0.00";
    }

    public static void main(String[] args) {
        System.out.println(formatToDouble(22.255));
    }
}
