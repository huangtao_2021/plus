package com.plus.utils;

import java.io.*;

/**
 * @program: plus-easy
 * @ClassName IOCloseUtil
 * @description: 流关闭帮助类
 * @author: AI
 * @create: 2023-11-23 10:44
 * @Version 1.0
 **/
public class IOCloseUtil {

    public static void close(InputStream in){
        if(in != null){
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void close(OutputStream out){
        if(out != null){
            try {
                out.flush();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void close(Reader r){
        if(r != null){
            try {
                r.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void close(Writer w){
        if(w != null){
            try {
                w.flush();
                w.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
