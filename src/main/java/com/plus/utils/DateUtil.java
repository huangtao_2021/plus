package com.plus.utils;


import org.springframework.util.StringUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;

/**
 * @Description :时间处理工具
 */
public class DateUtil {

    public static final String DATETIME_FORMATTER = "yyyy-MM-dd HH:mm:ss";

    public static final String DATEStr_FORMATTER = "yyyyMMdd";

    public static final String TimeStr_FORMATTER = "yyyyMMddHHmmss";

    public static final String DATE_FORMATTER = "yyyy-MM-dd";

    public static final String TIME_FORMATTER = "HH:mm:ss";

    public static final String DATETIME_T_FORMATTER = "yyyy-MM-dd'T'HH:mm:ss";

    /**
     * 获取当前时间时间戳（long）
     *
     * @return
     */
    public static long currentTimeMillis() {
        return System.currentTimeMillis();
    }

    public static long currentTimeSecond() {
        return System.currentTimeMillis()/1000;
    }

    /**
     * 获取当前日期(yyyy-MM-dd)
     *
     * @return
     */
    public static LocalDate currentLocalDate() {
        return LocalDate.now();
    }

    /**
     * 获取当前时间(HH:mm:ss.SSS)
     *
     * @return
     */
    public static LocalTime currentLocalTime() {
        return LocalTime.now();
    }

    /**
     * 获取当前日期时间(yyyy-MM-dd'T'HH:mm:ss.SSS)
     *
     * @return
     */
    public static LocalDateTime now() {
        return LocalDateTime.now();
    }

    /**
     * 获取当前日期字符串(yyyy-MM-dd)
     *
     * @return
     */
    public static String getCurrentDateStr() {
        return DateTimeFormatter.ofPattern(DATE_FORMATTER).format(now());
    }

    /**
     * 获取当前日期字符串(yyyyMMdd)
     *
     * @return
     */
    public static String getCurrentDateNohStr() {
        return DateTimeFormatter.ofPattern(DATEStr_FORMATTER).format(now());
    }


    /**
     * 获取当前时间字符串(HH:mm:ss)
     *
     * @return
     */
    public static String getCurrentTimeStr() {
        return DateTimeFormatter.ofPattern(TIME_FORMATTER).format(now());
    }

    public static String yyyymmddToyyyy_mm_dd(String date) {
        return LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyyMMdd")).format(DateTimeFormatter.ofPattern(DATE_FORMATTER));
    }

    /**
     * 获取当前日期时间字符串(yyyy-MM-dd HH:mm:ss)
     *
     * @return
     */
    public static String getCurrentDateTimeStr() {
        return DateTimeFormatter.ofPattern(DATETIME_FORMATTER).format(now());
    }

    /**
     * 将时间字符串转为自定义时间格式的LocalDateTime
     *
     * @param time   需要转化的时间字符串
     * @param format 自定义的时间格式
     * @return
     */
    public static LocalDateTime convertStringToLocalDateTime(String time, String format) {
        return LocalDateTime.parse(time, DateTimeFormatter.ofPattern(format));
    }
    /**
     * 将时间字符串转为自定义时间格式的LocalDateTime
     *  格式：yyyy-MM-dd HH:mm:ss
     * @param time   需要转化的时间字符串
     * @return
     */
    public static LocalDateTime convertStringToLocalDateTime(String time) {
        return LocalDateTime.parse(time, DateTimeFormatter.ofPattern(DATETIME_FORMATTER));
    }


    /**
     * 将时间字符串转为自定义时间格式的LocalDate
     *
     * @param time   需要转化的时间字符串
     * @param format 自定义的时间格式
     * @return
     */
    public static LocalDate convertStringToLocalDate(String time, String format) {
        return LocalDate.parse(time, DateTimeFormatter.ofPattern(format));
    }

    /**
     * 将时间字符串转为自定义时间格式的LocalDate
     *
     * @param time   需要转化的时间字符串
     * @param format 自定义的时间格式
     * @return
     */
    public static String convertLocalDateToString(LocalDate time, String format) {
        return time.format(DateTimeFormatter.ofPattern(format));
    }

    /**
     * 将LocalDateTime转为自定义的时间格式的字符串
     *
     * @param localDateTime 需要转化的LocalDateTime
     * @param format        自定义的时间格式
     * @return
     */
    public static String convertLocalDateTimeToString(LocalDateTime localDateTime, String format) {
        return localDateTime.format(DateTimeFormatter.ofPattern(format));
    }

    /**
     * 将LocalDateTime转为自定义的时间格式的字符串
     *格式：yyyy-MM-dd HH:mm:ss
     * @param localDateTime 需要转化的LocalDateTime
     * @return
     */
    public static String convertLocalDateTimeToString(LocalDateTime localDateTime) {
        return localDateTime.format(DateTimeFormatter.ofPattern(DATETIME_FORMATTER));
    }


    /**
     * 将long类型的timestamp转为LocalDateTime
     *
     * @param timestamp
     * @return
     */
    public static LocalDateTime convertTimestampToLocalDateTime(long timestamp) {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(timestamp), ZoneId.systemDefault());
    }

    /**
     * 将LocalDateTime转为long类型的timestamp
     *
     * @param localDateTime
     * @return
     */
    public static long convertLocalDateTimeToTimestamp(LocalDateTime localDateTime) {
        return localDateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }

    /**
     * 将LocalDateTime转为long类型的timestamp
     *
     * @param localDateTime
     * @return
     */
    public static long getTimestamp(String localDateTime) {
        return convertStringToLocalDateTime(localDateTime, DATETIME_FORMATTER).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }

    /**
     * 将LocalDate转为long类型的timestamp
     *
     * @param localDate
     * @return
     */
    public static long getTimestampByDate(String localDate) {
        return convertStringToLocalDate(localDate, DATE_FORMATTER).atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }

    /**
     * 获取LocalDateTime的最大时间的字符串格式(yyyy-MM-dd HH:mm:ss)
     *
     * @param localDateTime
     * @return
     */
    public static String getMaxDateTime(LocalDateTime localDateTime) {
        return convertLocalDateTimeToString(localDateTime.with(LocalTime.MAX), DATETIME_FORMATTER);
    }

    /**
     * 获取LocalDateTime的最小时间的字符串格式(yyyy-MM-dd HH:mm:ss)
     *
     * @param localDateTime
     * @return
     */
    public static String getMinDateTime(LocalDateTime localDateTime) {
        return convertLocalDateTimeToString(localDateTime.with(LocalTime.MIN), DATETIME_FORMATTER);
    }

    /**
     * 获取LocalDate的最大时间的字符串格式(yyyy-MM-dd HH:mm:ss)
     *
     * @param localDate
     * @return
     */
    public static String getMaxDateTime(LocalDate localDate) {
        return convertLocalDateTimeToString(localDate.atTime(LocalTime.MAX), DATETIME_FORMATTER);

    }

    /**
     * 获取LocalDate的最小时间的字符串格式(yyyy-MM-dd HH:mm:ss)
     *
     * @param localDate
     * @return
     */
    public static String getMinDateTime(LocalDate localDate) {
        return convertLocalDateTimeToString(localDate.atTime(LocalTime.MIN), DATETIME_FORMATTER);
    }

    /**
     * 获取当前距离日期
     *
     * @param index
     * @return
     */
    public static LocalDate getDay(int index) {
        return LocalDate.now().plusDays(index);
    }

    /**
     * 获取当前距离日期
     *
     * @param index
     * @return
     */
    public static String getDayString(int index) {
        return DateTimeFormatter.ofPattern(DATE_FORMATTER).format(LocalDate.now().plusDays(index));
    }

    /**
     * 获取当前距离日期
     *
     * @param index
     * @return
     */
    public static String getMonthSet(int index) {
        return DateTimeFormatter.ofPattern(DATETIME_FORMATTER).format(LocalDateTime.now().plusMonths(index));
    }

    /**
     * 获取当前距离日期
     *
     * @param index
     * @return
     */
    public static String getHourSet(int index) {
        return DateTimeFormatter.ofPattern(DATETIME_FORMATTER).format(LocalDateTime.now().plusHours(index));
    }

    /**
     * 获取当前距离时间
     *
     * @param index
     * @return
     */
    public static String getTimeString(int index) {
        return DateTimeFormatter.ofPattern(DATETIME_FORMATTER).format(LocalDateTime.now().plusDays(index));
    }

    /**
     * 获取当前距离分钟
     *
     * @param index
     * @return
     */
    public static String getMinuteString(int index) {
        return DateTimeFormatter.ofPattern(DATETIME_FORMATTER).format(LocalDateTime.now().plusMinutes(index));
    }

    /**
     * 计算秒数
     * @param time
     * @param timeOne
     * @return
     */
    public static long getDateSubSeconds(LocalDateTime time, LocalDateTime timeOne) {
        //计算秒数
        return Duration.between(time, timeOne).toMillis() / 1000;
    }


    /**
     * 将yyyy-MM-dd HH:mm:ss
     *
     * @param dataTime
     * @param format
     * @return
     */
    public static String stringToString(String dataTime, String format) {
        if (StringUtils.isEmpty(dataTime)) {
            return "";
        }
        if (dataTime.length() < 19) {
            dataTime += " 00:00:00";
        }
        return convertStringToLocalDateTime(dataTime.substring(0, 19), DATETIME_FORMATTER).format(DateTimeFormatter.ofPattern(StringUtils.isEmpty(format) ? DATETIME_FORMATTER : format));
    }

    public static void main(String[] args) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dataTime = "2023-08-23 00:00";

        System.out.println(dataTime.length());
        System.out.println(LocalDateTime.parse(dataTime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")));
        //System.out.println(stringToString("2022-11-22 17:26:00.000", "yyyy-MM-dd HH:mm:ss"));


    }
}
