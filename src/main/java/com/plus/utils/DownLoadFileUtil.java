package com.plus.utils;


import cn.hutool.core.io.FileUtil;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.util.StringUtils;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;

/**
 * @Description :文件下载处理工具
 */
public class DownLoadFileUtil {

    /**
     * 浏览器下载指定文件
     * @param path 完整物理路径
     * @param setFileName 文件名称，完整名称，如：程序猿的自我修养.pdf,为空默认取文件本身的名称
     * @param isDel 导出后是否删除磁盘文件,方法调用之前的这个文件的流必须先关闭掉才有生效
     * @return
     */
    public static HttpServletResponse downloadFile(HttpServletResponse response, String path, String setFileName,
                                                   boolean isDel) {
        InputStream fis = null;
        OutputStream toClient = null;
        File file = null;
        try {
            // path是指欲下载的文件的路径。
            file = new File(path);
            // 取得文件名。
            String filename = file.getName();
            if(StringUtils.isEmpty(setFileName)){
                setFileName = filename;
            }
            // 取得文件的后缀名。
            //String ext = filename.substring(filename.lastIndexOf(".") + 1).toUpperCase();
            // 以流的形式下载文件。
            fis = new BufferedInputStream(new FileInputStream(path));
            byte[] buffer = new byte[fis.available()];
            fis.read(buffer);
            // 清空response
            response.reset();
            // 设置response的Header
            response.addHeader("Content-Disposition", "attachment;filename=" + java.net.URLDecoder.decode(setFileName, "UTF-8"));
            response.addHeader("Content-Length", "" + file.length());
            toClient = new BufferedOutputStream(response.getOutputStream());
            response.setContentType("application/octet-stream");
            toClient.write(buffer);
        } catch (IOException ex) {
            ex.printStackTrace();
        }finally {
            IOCloseUtil.close(fis);
            IOCloseUtil.close(toClient);
            //下载之后删除
            if(isDel && FileUtil.exist(file)){
                FileUtil.del(file);
            }
        }
        return response;
    }

    /**
     * 下载网络文件
     * @param url http文件链接
     * @param filename 文件名称,完整名称，如：程序猿的自我修养.pdf
     */
    public static void downloadNet(HttpServletResponse response,String url,String filename) {
        InputStream inStream = null;
        try {
            URL url2 = new URL(url);
            URLConnection conn = url2.openConnection();
            inStream = conn.getInputStream();
            byte[] buffer = new byte[1204];
            int bytesum = 0;
            int byteread = 0;
            response.addHeader("Content-Disposition", "attachment;filename=" + new String(filename.getBytes()));
            response.setContentType("application/octet-stream");
            while ((byteread = inStream.read(buffer)) != -1) {
                bytesum += byteread;
                response.getOutputStream().write(buffer,0,byteread);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            IOCloseUtil.close(inStream);
        }
    }
}
