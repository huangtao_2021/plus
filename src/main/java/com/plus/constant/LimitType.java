package com.plus.constant;

/**
 * @program: plus
 * @ClassName LimitType
 * @description: 限流方式
 * @author: AI
 * @create: 2023-11-07 13:56
 * @Version 1.0
 **/
public enum LimitType {
    /**
     * 默认策略全局限流
     */
    DEFAULT,
    /**
     * 根据请求者IP进行限流
     */
    IP
}

