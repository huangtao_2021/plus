package com.plus.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.plus.base.BaseEntity;
import io.swagger.models.auth.In;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.NoArgsConstructor;

/**
 * 白名单实体类
 *
 * @author AI
 * @since 2023-11-07
 */
@Data
@ApiModel(value = "Whiteip对象", description = "白名单")
@TableName("whiteip")
@NoArgsConstructor
public class Whiteip{

    private static final long serialVersionUID = 1L;

  @TableId(value = "id", type = IdType.AUTO)
  private Integer id;
  private String ipAddress;
    /**
     * 1启用  2关闭
     */
  @ApiModelProperty(value = "1启用  2关闭")
  private Integer status;


  public Whiteip(String ipAddress, Integer status){
    this.ipAddress = ipAddress;
    this.status = status;
  }


}
