package com.plus.dto.custom;

import com.deepoove.poi.data.TableRenderData;
import lombok.Data;

import java.util.ArrayList;

/**
 * @program: AI
 * @ClassName TableData
 * @description:
 * @author: 黄涛
 * @create: 2023-10-31 11:00
 * @Version 1.0
 **/
@Data
public class TableData {

    /**
     * 标题
     */
    private String title;

    /**
     * 表格
     */
    private TableRenderData table;

    private String[][] tableList;

    /**
     * 总价
     */
    private String totalPrice;

    /**
     * 列表
     */
    private ArrayList<Object> itemList;

}
