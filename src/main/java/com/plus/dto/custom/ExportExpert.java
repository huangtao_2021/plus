package com.plus.dto.custom;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @program: sxsoft_expert
 * @ClassName ExportExpert
 * @description:
 * @author: AI
 * @create: 2023-08-21 15:10
 * @Version 1.0
 **/
@Data
@Builder
@EqualsAndHashCode
public class ExportExpert implements Serializable {

    @ColumnWidth(20)
    @ExcelProperty("ID")
    private String ID;
    @ColumnWidth(20)
    @ExcelProperty("DEPARTID")
    private String DEPARTID;
    @ColumnWidth(20)
    @ExcelProperty("姓名")
    private String xm;
    @ColumnWidth(20)
    @ExcelProperty("常驻地")
    private String czd;
    @ColumnWidth(40)
    @ExcelProperty("公共资源评审品目")
    private String ggzypspm;
    @ExcelProperty("政府采购评审品目")
    @ColumnWidth(40)
    private String zfcgpspm;
    @ExcelProperty("职称有效日期")
    @ColumnWidth(20)
    private String zcyxrq;
    @ColumnWidth(40)
    @ExcelProperty("联系电话")
    private String lxdh;
    @ColumnWidth(40)
    @ExcelProperty("是否列入黑名单")
    private String sflrhmd;
    @ColumnWidth(40)
    @ExcelProperty("公共资源专家来源")
    private String ggzyzjly;
    @ColumnWidth(40)
    @ExcelProperty("政府采购专家来源")
    private String zfcgzjly;
    @ColumnWidth(20)
    @ExcelProperty("专家类型")
    private String zjlx;
    @ColumnWidth(20)
    @ExcelProperty("职称专业")
    private String zczy;
    @ColumnWidth(20)
    @ExcelProperty("职称级别")
    private String zcjb;
    @ColumnWidth(20)
    @ExcelProperty("性别")
    private String xb;
    @ColumnWidth(20)
    @ExcelProperty("证件类型")
    private String zjlx2;
    @ColumnWidth(20)
    @ExcelProperty("证件号码")
    private String zjhm;
    @ColumnWidth(20)
    @ExcelProperty("在职情况")
    private String zzqk;
    @ColumnWidth(20)
    @ExcelProperty("工作单位")
    private String gzdw;
    @ColumnWidth(40)
    @ExcelProperty("需回避的其他单位")
    private String xhbddqtyq;
    @ColumnWidth(30)
    @ExcelProperty("是否可抽取")
    private String sfkcq;
    @ColumnWidth(20)
    @ExcelProperty("专家编码")
    private String zjbm;

}
