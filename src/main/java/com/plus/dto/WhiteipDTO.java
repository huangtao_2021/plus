package com.plus.dto;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.plus.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.plus.common.CommonDto;
/**
 * 白名单数据传输对象实体类
 *
 * @author AI
 * @since 2023-11-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "Whiteip对象", description = "白名单")
public class WhiteipDTO extends CommonDto {

					private Integer id;
					private String ipAddress;
		/**
		 * 1启用  2关闭
		 */
			@ApiModelProperty(value = "1启用  2关闭")
					private Integer status;


		}
