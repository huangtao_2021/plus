package com.plus.exception;

import com.plus.common.R;
import com.plus.constant.ErrorCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * @ClassName: GlobalControllerExceptionHandler
 * @Description:全局异常处理器
 */
@ControllerAdvice
@Slf4j
public class GlobalControllerExceptionHandler {



    //region 拦截捕捉自定义异常
    /**
     * 业务异常
     */
    @ExceptionHandler(ServiceException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public R handleServiceException(ServiceException ex) {
        log.error(ex.getMessage(), ex);
        return R.data(ErrorCode.NOT_REPEAT_REQUEST.getErrorCode(),"",ex.getMessage());
    }
    //endregion



    //region 捕捉参数校验异常
    @ExceptionHandler(ValidateException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    private R ValidateExceptionHandler(ValidateException e) {
        return R.data(ErrorCode.SYSTEM_EXCEPTION.getErrorCode(),"", e.getMessage());
    }
    @ExceptionHandler(BindException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    private R BindException(BindException e) {
        return R.data(ErrorCode.SYSTEM_EXCEPTION.getErrorCode(),"", Objects.requireNonNull(e.getFieldError()).getDefaultMessage());
    }
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    private R MethodArgumentNotValidException(MethodArgumentNotValidException e) {
        return R.data(ErrorCode.SYSTEM_EXCEPTION.getErrorCode(),"", e.getBindingResult().getFieldError().getDefaultMessage());
    }
    //endregion



    //region 全局异常捕捉处理
    /**
     * 全局异常捕捉处理
     */
    @ResponseBody
    @ExceptionHandler(value = Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public R handleException(Exception e, HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        log.error("请求地址:{},发生系统异常。", requestURI, e);
        return R.data(ErrorCode.SYSTEM_EXCEPTION.getErrorCode(),"",e.getMessage());
    }


    /**
     * 运行时异常
     */
    @ExceptionHandler(RuntimeException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public R handleRuntimeException(Exception e, HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        log.error("请求地址:{},发生运行时异常。", requestURI, e);
        return R.data(ErrorCode.SYSTEM_EXCEPTION.getErrorCode(),"",e.getMessage());
    }
    //endregion




    //region 异步处理方法
    /**
     * 异步处理方法
     * @param e
     */
    @Async
    public void sendMessage(Exception e) {
        //doing
    }
    //endregion


}
