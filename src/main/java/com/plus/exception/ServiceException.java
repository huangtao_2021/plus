package com.plus.exception;


/**
 * @program: plus
 * @ClassName ServiceException
 * @description:自定义通用异常
 * @author: AI
 * @create: 2023-11-07 16:04
 * @Version 1.0
 **/
public final class ServiceException extends BaseException {

    private static final long serialVersionUID = 1L;


    public ServiceException(String errorMsg) {
        super(errorMsg);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }

    public ServiceException(String errorCode, String errorMsg) {
        super(errorCode, errorMsg);
    }

    public ServiceException(Throwable cause, String errorMsg) {
        super(cause, errorMsg);
    }

    public ServiceException(Throwable cause, String errorCode, String errorMsg) {
        super(cause, errorCode, errorMsg);
    }

}

