package com.plus.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @program: plus
 * @ClassName InterceptorConfig
 * @description:
 * InterceptorConfig 是一个配置类，用于添加拦截器。
 * 在这个类中，我们可以配置需要拦截的接口路径以及排除不需要拦截的接口路径。
 * 在这个例子中，我们添加了JWTInterceptor拦截器来对请求进行token验证，
 * 并设置了"/user/test"接口需要进行验证，而"/user/login"接口则被排除在验证之外，即所有用户都放行登录接口。
 * @author: AI
 * @create: 2023-11-02 08:26
 * @Version 1.0
 **/
@Configuration
public class InterceptorConfig implements WebMvcConfigurer {

    @Value("${spring.jwtToken.filterPaths}")
    private String filterPaths;

    @Value("${spring.jwtToken.excludePaths}")
    private String excludePaths;

    /**
     * 添加拦截器配置
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        String[] filterPathsArr = filterPaths.split(",");
        String[] excludePathsArr = excludePaths.split(",");

        registry.addInterceptor(new JWTInterceptor())
                .addPathPatterns(filterPathsArr)         // 如对"/user/login"接口进行token验证，"/**"拦截所有请求，包括静态资源文件
                .excludePathPatterns(excludePathsArr);  // 所有用户都放行登录接口
    }
}


