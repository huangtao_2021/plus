//package com.plus.config;
//
//import lombok.extern.slf4j.Slf4j;
//import org.apache.activemq.command.ActiveMQMessage;
//import org.springframework.jms.annotation.JmsListener;
//import org.springframework.jms.annotation.JmsListeners;
//import org.springframework.stereotype.Component;
//
//import javax.jms.JMSException;
//import javax.jms.Session;
//
///**
// * topic模式/广播模式/发布订阅模式 一对多，多个消费者可同时接收到消息
// * topic模式无死信队列，死信队列是queue模式
// * containerFactory属性的值关联config类中的声明
// * 消息存储规则：所有消费者消费成功，MQ服务端消息删除，有一个消息没有没有消费完成，消息也会存储在MQ服务端。
// *
// */
//
//@Component
//@Slf4j
//public class TopicListener {
//
//    @JmsListener(destination = "active.topic", containerFactory = "jmsListenerContainerTopic")
//   // @JmsListeners(value = {@JmsListener(destination = "test_0001"), @JmsListener(destination = "test_0002")}) //多队列监听
//    public void topicListener(ActiveMQMessage message, Session session, String msg) throws JMSException {
//        try {
//            log.info("activeTopic接收到消息:{}",msg);
//            //手动签收
//            message.acknowledge();
//        } catch (Exception e) {
//            //重新发送
//            session.recover();
//        }
//    }
//
////    /**
////     * topicListener和topicListener2两个方法都会接口到消息
////     * @param message
////     * @param session
////     * @param msg
////     * @throws JMSException
////     */
////    @JmsListener(destination = "active.topic", containerFactory = "jmsListenerContainerTopic")
////    public void topicListener2(ActiveMQMessage message, Session session, String msg) throws JMSException {
////        try {
////            log.info("activeTopic2接收到消息:{}",msg);
////            //手动签收
////            message.acknowledge();
////        } catch (Exception e) {
////            //重新发送
////            session.recover();
////        }
////    }
//
//}