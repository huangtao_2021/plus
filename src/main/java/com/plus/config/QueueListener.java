//package com.plus.config;
//
//import lombok.extern.slf4j.Slf4j;
//import org.apache.activemq.command.ActiveMQMessage;
//import org.springframework.jms.annotation.JmsListener;
//import org.springframework.stereotype.Component;
//
//import javax.jms.JMSException;
//import javax.jms.Session;
//
///**
// * queue 模式 单对单，两个消费者监听同一个队列则通过轮询接收消息,消息遵循先到先得，消息只能被一个消费者消费。
// * containerFactory属性的值关联config类中的声明
// * 消息存储规则：消费者消费消息成功，MQ服务端消息删除
// *
// */
//
//@Component
//@Slf4j
//public class QueueListener {
//
//    @JmsListener(destination = "active.queue", containerFactory = "jmsListenerContainerQueue")
//    // @JmsListeners(value = {@JmsListener(destination = "test_0001"), @JmsListener(destination = "test_0002")}) //多队列监听
//    public void queueListener(ActiveMQMessage message, Session session, String msg) throws JMSException {
//        try {
//            log.info("activeQueue接收到消息:{}",msg);
//            //手动签收
//            message.acknowledge();
//        } catch (Exception e) {
//            //重新发送
//            session.recover();
//        }
//    }
//
//
//}
