package com.plus.config;

import com.github.benmanes.caffeine.cache.Caffeine;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.caffeine.CaffeineCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

/**
 * @program: plus
 * @ClassName CaffeineCacheConfig
 * @description: 本地/内存缓存配置类
 * @author: AI
 * @create: 2023-11-09 15:14
 * @Version 1.0
 **/
@Configuration
@EnableCaching
public class CaffeineCacheConfig {
    @Bean
    public CacheManager cacheManager(){
        CaffeineCacheManager cacheManager = new CaffeineCacheManager();
        //Caffeine配置
        Caffeine<Object, Object> caffeine = Caffeine.newBuilder()
                //最后一次写入后经过固定时间过期
                .expireAfterWrite(10, TimeUnit.SECONDS)
                //maximumSize=[long]: 缓存的最大条数
                .maximumSize(1000);
        cacheManager.setCaffeine(caffeine);
        return cacheManager;
    }
}


//问题：好像不能动态设置过期时间，比如部门信息30分钟后过期，用户信息10秒后过期，怎么配置？？？


//配置说明
//initialCapacity=[integer]: 初始的缓存空间大小
//maximumSize=[long]: 缓存的最大条数
//maximumWeight=[long]: 缓存的最大权重
//expireAfterAccess=[duration]: 最后一次写入或访问后经过固定时间过期
//expireAfterWrite=[duration]: 最后一次写入后经过固定时间过期
//refreshAfterWrite=[duration]: 创建缓存或者最近一次更新缓存后经过固定的时间间隔，刷新缓存
//weakKeys: 打开key的弱引用
//weakValues：打开value的弱引用
//softValues：打开value的软引用
//recordStats：开发统计功能 注意：
//expireAfterWrite和expireAfterAccess同事存在时，以expireAfterWrite为准。
//maximumSize和maximumWeight不可以同时使用
//weakValues和softValues不可以同时使用




//使用
//直接再接口上配置注解接口，示例如下：

//@Cacheable(cacheNames = "user",key = "#dto.userId")
//Cacheable: @Cacheble注解表示这个方法有了缓存的功能，方法的返回值会被缓存下来，下一次调用该方法前，会去检查是否缓存中已经有值，如果有就直接返回，不调用方法

//@CacheEvict(cacheNames = "user",key = "#dto.userId")
//@CacheEvict: @CacheEvict注解的方法，会清空指定缓存。一般用在更新或者删除的方法上。key必须和查询的一样清缓存才能生效。


//@CachePut(cacheNames = "user",key = "#dto.userId")
//@CachePut : @CachePut注解的方法，保证方法被调用，又希望结果被缓存。会把方法的返回值put到缓存里面缓存起来。它通常用在新增方法上。key必须和查询的一样清缓存才能生效。





