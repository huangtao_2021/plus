package com.plus.config;

import com.plus.constant.LimitType;

import java.lang.annotation.*;

/**
 * @program: plus
 * @ClassName Limit
 * @description:限流注解定义
 * @author: AI
 * @create: 2023-11-07 13:56
 * @Version 1.0
 **/
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RateLimiter {
    /**
     * 限流key
     */
    String key() default "rate_limit:";

    /**
     * 限流时间,单位秒
     */
    int time() default 60;

    /**
     * 限流次数
     */
    int count() default 100;

    /**
     * 限流类型
     */
    LimitType limitType() default LimitType.DEFAULT;
}

