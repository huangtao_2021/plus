package com.plus.service.impl;


import com.plus.entity.Whiteip;
import com.plus.mapper.WhiteipMapper;
import com.plus.service.IWhiteipService;
import com.plus.dto.WhiteipDTO;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.plus.common.Condition;
import com.plus.utils.BeanCopyUtils;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.AllArgsConstructor;
import cn.hutool.core.util.StrUtil;
import java.util.List;
import java.util.ArrayList;

/**
 * 白名单 服务实现类
 *
 * @author AI
 * @since 2023-11-07
 */
@Service
@AllArgsConstructor
public class WhiteipServiceImpl extends ServiceImpl<WhiteipMapper, Whiteip> implements IWhiteipService {

    protected WhiteipMapper whiteipMapper;
    @Override
    public IPage<Whiteip> page(WhiteipDTO dto) {
        IPage<Whiteip> page = Condition.getPage(dto);
        QueryWrapper<Whiteip> queryWrapper = Condition.getQueryWrapper(BeanCopyUtils.copy(dto, Whiteip.class));
        if (StrUtil.isNotEmpty(dto.getColumn()) && StrUtil.isNotEmpty(dto.getKeywords())) {
        queryWrapper.like(dto.getColumn(),dto.getKeywords());
        }
        if (StrUtil.isNotEmpty(dto.getOrderAsc())) {
        queryWrapper.orderByAsc(dto.getOrderAsc());
        }
        if (StrUtil.isNotEmpty(dto.getOrderDesc())) {
        queryWrapper.orderByDesc(dto.getOrderDesc());
        }
        if (dto.getStartTime() != null) {
        queryWrapper.gt("created",dto.getStartTime());
        }
        if (dto.getEndTime() != null) {
        queryWrapper.lt("created",dto.getEndTime());
        }
        return whiteipMapper.selectPage(page,queryWrapper);
    }

    @Override
    public List<Whiteip> list(WhiteipDTO dto) {
        QueryWrapper<Whiteip> queryWrapper = Condition.getQueryWrapper(BeanCopyUtils.copy(dto, Whiteip.class));
        if (StrUtil.isNotEmpty(dto.getColumn()) && StrUtil.isNotEmpty(dto.getKeywords())) {
        queryWrapper.like(dto.getColumn(),dto.getKeywords());
        }
        if (StrUtil.isNotEmpty(dto.getOrderAsc())) {
        queryWrapper.orderByAsc(dto.getOrderAsc());
        }
        if (StrUtil.isNotEmpty(dto.getOrderDesc())) {
        queryWrapper.orderByDesc(dto.getOrderDesc());
        }
        if (dto.getStartTime() != null) {
        queryWrapper.gt("created",dto.getStartTime());
        }
        if (dto.getEndTime() != null) {
        queryWrapper.lt("created",dto.getEndTime());
        }
        return whiteipMapper.selectList(queryWrapper);
    }

    @Override
    public Integer save(WhiteipDTO dto) {
        return whiteipMapper.insert(BeanCopyUtils.copy(dto,Whiteip.class));
    }

    @Override
    public boolean saveBatch(List<WhiteipDTO> dtoList) {
        List<Whiteip> list = new ArrayList<>();
        for(WhiteipDTO dto : dtoList){
           list.add(BeanCopyUtils.copy(dto,Whiteip.class));
        }
        return saveBatch(list);
    }

    @Override
    public Integer updateById(WhiteipDTO dto) {
        return whiteipMapper.updateById(BeanCopyUtils.copy(dto,Whiteip.class));
    }


    @Override
    public Integer deleteLogic(List<Integer> toIntList) {
        return whiteipMapper.deleteBatchIds(toIntList);
    }

    @Override
    public Whiteip getOne(WhiteipDTO dto) {
        return whiteipMapper.selectOne(Condition.getQueryWrapper(BeanCopyUtils.copy(dto,Whiteip.class)));
    }


    /**
     * 批量插入
     * @param list
     * @return
     */
    @Override
    public Integer insertSelective(List<Whiteip> list) {
        return whiteipMapper.insertSelective(list);
    }
}
