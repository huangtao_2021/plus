package com.plus.service;

import com.plus.entity.Whiteip;
import com.plus.dto.WhiteipDTO;
import java.util.List;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * 白名单 服务类
 *
 * @author AI
 * @since 2023-11-07
 */
public interface IWhiteipService  {

        IPage<Whiteip> page(WhiteipDTO dto) ;

        List<Whiteip> list(WhiteipDTO dto) ;

        Integer save(WhiteipDTO dto) ;

        boolean saveBatch(List<WhiteipDTO> dtoList);

        Integer updateById(WhiteipDTO dto) ;

        Integer deleteLogic(List<Integer> toIntList) ;

        Whiteip getOne(WhiteipDTO dto) ;

        /**
         * 批量插入
         * @param list
         * @return
         */
        Integer insertSelective(List<Whiteip> list);
}
