package com.plus.mapper;

import com.plus.entity.Whiteip;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;

/**
 * 白名单 Mapper 接口
 *
 * @author AI
 * @since 2023-11-07
 */
public interface WhiteipMapper extends BaseMapper<Whiteip> {

    /**
     * 批量插入，使用默认的批量插入基本够用
     * @param list
     * @return
     */
    Integer insertSelective(List<Whiteip> list);
}
